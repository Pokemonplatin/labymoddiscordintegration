package sexy.poke.labymod.data;

import com.github.psnrigner.discordrpcjava.DiscordRichPresence;
import net.labymod.api.events.TabListEvent;
import net.labymod.utils.ServerData;
import sexy.poke.labymod.DiscordAddon;

import java.util.regex.Pattern;

/**
 * Created by Flo on 31.12.2017
 */
public abstract class BaseDataGrabber {

    private String[] domains;

    protected String currentMode = "";

    public BaseDataGrabber(String... domains) {
        this.domains = domains;
    }

    public boolean isOnServer(ServerData data) {
        if (data != null)
            return isOnServer(data.getIp());
        return false;
    }

    public boolean isOnServer(String server) {
        for (String str : domains) {
            if (server.toLowerCase().endsWith(str))
                return true;
        }
        return false;
    }

    public void updatePresence(String details, String state) {
        DiscordAddon.instance.updatePresence(details,state);
    }

    public abstract void checkScoreboard();

    public abstract void checkTabList(TabListEvent.Type type, String unformattedString);

    public static String getTextWithoutFormattingCodes(String text) {
        return text == null ? null : FORMATTING_CODE_PATTERN.matcher(text).replaceAll("");
    }

    private static final Pattern FORMATTING_CODE_PATTERN = Pattern.compile("(?i)" + String.valueOf('\u00a7') + "[0-9A-FK-OR]");
}
