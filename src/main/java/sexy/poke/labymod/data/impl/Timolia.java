package sexy.poke.labymod.data.impl;

import com.github.psnrigner.discordrpcjava.DiscordRichPresence;
import net.labymod.api.events.TabListEvent;
import sexy.poke.labymod.data.BaseDataGrabber;

/**
 * Created by Flo on 31.12.2017
 */
public class Timolia extends BaseDataGrabber {

    public Timolia() {
        super("timolia.de");
    }

    //TODO Language

    @Override
    public void checkScoreboard() {

    }

    @Override
    public void checkTabList(TabListEvent.Type type, String unformattedString) {
        if (type == TabListEvent.Type.HEADER) {

            String msg = unformattedString.split("\n")[1];

            msg = msg.substring(0, msg.length() - 2).replace(" Du spielst auf ", "");

            String id = msg.replaceAll("[^0-9]", "");
            String game = "";

            if (msg.matches("games[0-9][1-9]")) {
                game = "Lobby";
            } else if (msg.matches("pvp[0-9][1-9]")) {
                game = "1v1";
            } else if (msg.matches("jumpworld[0-9][1-9]")) {
                game = "Jumpworld";
            } else if (msg.matches("4rena[0-9][0-9][1-9]")) {
                game = "4rena";
                //remove the 4 in 4rena
                id = id.substring(1);
            } else if (msg.matches("intime[0-9][1-9]")) {
                game = "Intime";
            } else if (msg.matches("splun[0-9][1-9]")) {
                game = "Splun";
            } else if (msg.matches("arcade[0-9][1-9]")) {
                game = "Arcade";
            } else if (msg.matches("dna[0-9][1-9]")) {
                game = "Dna";
            } else if (msg.matches("castles[A-Z]{2}x[A-Z]{2}[0-9][0-9][1-9]")) {
                msg = msg.replace("castles", "");
                msg = msg.replace(id, "");
                game = "Castles " + msg;
            } else if (msg.matches("brainbow[0-9][1-9]")) {
                game = "Brainbow";
            } else if (msg.matches("mineception[0-9][0-9][0-9][0-9]")) {
                game = "Mineception";
            }


            if (game.equalsIgnoreCase("Lobby")) {
                updatePresence("Auf Timolia.de", "In Lobby " + id);
            } else {
                updatePresence("Auf Timolia.de", "In " + game + " " + id);
            }
        }
    }
}
