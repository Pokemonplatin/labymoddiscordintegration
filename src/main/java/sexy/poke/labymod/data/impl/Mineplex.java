package sexy.poke.labymod.data.impl;

import com.github.psnrigner.discordrpcjava.DiscordRichPresence;
import net.labymod.api.events.TabListEvent;
import sexy.poke.labymod.data.BaseDataGrabber;

/**
 * Created by Flo on 31.12.2017
 */
public class Mineplex extends BaseDataGrabber {

    public Mineplex() {
        super("mineplex.com");
    }

    //TODO Language

    @Override
    public void checkScoreboard() {

    }

    @Override
    public void checkTabList(TabListEvent.Type type, String unformattedString) {
        if (type == TabListEvent.Type.HEADER) {
            String[] head = unformattedString.split("\n");

            String game = head[0];

            if (game.contains("Lobby")) {
                String id = game.replaceAll("[^0-9]", "");

                game = "Lobby " + id;
            }

            updatePresence("Auf Mineplex.com", "In " + game);
        }
    }
}
