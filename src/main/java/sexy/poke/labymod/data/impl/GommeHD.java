package sexy.poke.labymod.data.impl;

import com.github.psnrigner.discordrpcjava.DiscordRichPresence;
import net.labymod.api.events.TabListEvent;
import sexy.poke.labymod.data.BaseDataGrabber;

/**
 * Created by Flo on 31.12.2017
 */
public class GommeHD extends BaseDataGrabber {

    public GommeHD() {
        super("gommehd.net", "gommehd.com");
    }

    //TODO Language

    @Override
    public void checkScoreboard() {

    }

    @Override
    public void checkTabList(TabListEvent.Type type, String unformattedString) {
        if(type == TabListEvent.Type.HEADER) {
            String msg = unformattedString.split("\n")[0];

            msg = msg.replace("GommeHD.net ", "");

            updatePresence("Auf GommeHD.net", "In " + msg);
        }
    }
}
