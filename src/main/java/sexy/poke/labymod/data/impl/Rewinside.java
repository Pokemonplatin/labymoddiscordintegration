package sexy.poke.labymod.data.impl;

import net.labymod.api.events.TabListEvent;
import net.labymod.core.LabyModCore;
import sexy.poke.labymod.data.BaseDataGrabber;

/**
 * Created by Flo on 31.12.2017
 */
public class Rewinside extends BaseDataGrabber {
    public Rewinside() {
        super("rewinside.tv");
    }

    //TODO Language

    @Override
    public void checkScoreboard() {
        LabyModCore.getMinecraft().getWorld().Z().e().forEach(o -> {
            if (o.c() == 12) {
                String msg = getTextWithoutFormattingCodes(o.e());
                if (msg.startsWith("Lobby")) {
                    if (!currentMode.equalsIgnoreCase(msg)) {
                        currentMode = msg;

                        updatePresence("Auf Rewinside.tv","In Lobby " + msg.split("-")[1]);
                    }
                }
            }
        });
    }

    @Override
    public void checkTabList(TabListEvent.Type type, String unformattedString) {
        if (type == TabListEvent.Type.HEADER) {
            if (unformattedString.contains("-")) {
                unformattedString = unformattedString.replace("Du spielst auf ", "");

                String[] split = unformattedString.split("-");

                updatePresence("Auf Rewinside.tv","In " + split[0] + " " + split[1]);
            } else if(unformattedString.contains("rewinside.tv")) {
                currentMode = "";
            }
        }
    }
}
