package sexy.poke.labymod;

import com.github.psnrigner.discordrpcjava.*;
import net.labymod.api.LabyModAddon;
import net.labymod.core.LabyModCore;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.utils.ServerData;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import sexy.poke.labymod.data.BaseDataGrabber;
import sexy.poke.labymod.data.impl.GommeHD;
import sexy.poke.labymod.data.impl.Mineplex;
import sexy.poke.labymod.data.impl.Rewinside;
import sexy.poke.labymod.data.impl.Timolia;
import sexy.poke.labymod.util.TimeHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Flo on 30.12.2017
 */
public class DiscordAddon extends LabyModAddon {

    public static DiscordAddon instance;

    private DiscordRpc discordRpc;
    private DiscordEventHandler discordEventHandler;

    private ServerData currentData;

    private TimeHelper timer = new TimeHelper();

    private List<BaseDataGrabber> dataGrabbers = new ArrayList<>();

    @Override
    public void onEnable() {
        instance = this;

        getApi().registerForgeListener(this);

        dataGrabbers.addAll(Arrays.asList(
                new GommeHD(), new Rewinside(), new Timolia(),
                new Mineplex()));

        discordRpc = new DiscordRpc();

        discordEventHandler = new BasicDiscordEventHandler();

        setOnNoServer();

        getApi().getEventManager().register((type, a, b) -> {
            if (currentData == null)
                return;

            for (BaseDataGrabber bdg : dataGrabbers) {
                if (bdg.isOnServer(currentData)) {
                    bdg.checkTabList(type, getTextWithoutFormattingCodes(a));
                }
            }
        });

        getApi().getEventManager().registerOnJoin(serverData -> {
            try {
                if (serverData == null)
                    return;

                if (currentData != null && currentData.getIp().equalsIgnoreCase(serverData.getIp())) {
                    currentData = null;
                    setOnNoServer();
                    return;
                }

                currentData = serverData;

                for (BaseDataGrabber bdg : dataGrabbers) {
                    if (bdg.isOnServer(serverData)) {
                        //TODO Langage
                        updatePresence("Spielt auf: " + serverData.getIp(), "In Lobby");
                        return;
                    }
                }

                //TODO Language
                updatePresence("Spielt auf: " + serverData.getIp(), "(0__0)");

            } catch (Exception e) {
                e.printStackTrace();
            }
        });


        //labymod bug never called
        getApi().getEventManager().registerOnQuit(serverData -> {
            try {
                setOnNoServer();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void updatePresence(String details, String state) {
        DiscordRichPresence discordRichPresence = new DiscordRichPresence();
        discordRichPresence.setDetails(details);
        discordRichPresence.setState(state);

        discordRichPresence.setStartTimestamp(System.currentTimeMillis() / 1000L);
        discordRichPresence.setLargeImageKey("labymod");

        updatePresence(discordRichPresence);
    }


    public void updatePresence(DiscordRichPresence presence) {
        //TODO dont always create new Rpc
        if (discordRpc != null)
            discordRpc.shutdown();
        discordRpc = new DiscordRpc();
        discordRpc.init("396690938031898627", discordEventHandler, true, null);

        System.out.println(presence.getDetails());

        discordRpc.updatePresence(presence);
        discordRpc.runCallbacks();
    }

    public void setOnNoServer() {
        //TODO language
        updatePresence("Auf keinem Server", "(-o-)");
    }

    @Override
    public void onDisable() {
        discordRpc.shutdown();
    }

    @Override
    public void loadConfig() {
    }

    @Override
    protected void fillSettings(List<SettingsElement> list) {
    }

    @SubscribeEvent
    public void onRenderGameOverlay(final RenderGameOverlayEvent event) {

        //check scoreboard only once per second
        if (!timer.isDelayComplete(1000)) {
            return;
        }

        timer.setLastMS();

        //Tricky way to check the scoreboard
        if (currentData != null && LabyModCore.getMinecraft().getWorld() != null && LabyModCore.getMinecraft().getWorld().Z() != null /*Check if scoreboard is not null*/) {

            for (BaseDataGrabber bdg : dataGrabbers) {
                if (bdg.isOnServer(getApi().getCurrentServer())) {
                    bdg.checkScoreboard();
                    return;
                }
            }
        }
    }

    public static String getTextWithoutFormattingCodes(String text) {
        return text == null ? null : FORMATTING_CODE_PATTERN.matcher(text).replaceAll("");
    }

    private static final Pattern FORMATTING_CODE_PATTERN = Pattern.compile("(?i)" + String.valueOf('\u00a7') + "[0-9A-FK-OR]");

    private class BasicDiscordEventHandler implements DiscordEventHandler {

        @Override
        public void ready() {
        }

        @Override
        public void disconnected(ErrorCode errorCode, String message) {
        }

        @Override
        public void errored(ErrorCode errorCode, String message) {
        }

        @Override
        public void joinGame(String joinSecret) {
            System.out.println(joinSecret);
        }

        @Override
        public void spectateGame(String spectateSecret) {
            System.out.println(spectateSecret);
        }

        @Override
        public void joinRequest(DiscordJoinRequest joinRequest) {
        }
    }
}
