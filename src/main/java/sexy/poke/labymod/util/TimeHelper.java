package sexy.poke.labymod.util;

/**
 * Created by Flo on 31.12.2017
 */
public class TimeHelper {
    private long lastMS = 0L;

    public boolean isDelayComplete(long delay) {
        return System.currentTimeMillis() - this.lastMS >= delay;
    }

    public boolean isDelayComplete(float delay) {
        return System.currentTimeMillis() - this.lastMS >= delay;
    }

    public boolean isDelayComplete(int delay) {
        return System.currentTimeMillis() - this.lastMS >= delay;
    }

    public boolean isDelayComplete(double delay) {
        return System.currentTimeMillis() - this.lastMS >= delay;
    }

    public void setLastMS() {
        this.lastMS = System.currentTimeMillis();
    }
}
